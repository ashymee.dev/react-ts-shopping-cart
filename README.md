# Shopping Cart App (an example)

This is a simple example of `Shopping Cart` app using `React.js` with `TypeScript`.

### How to test this app on your local machine?

clone this repo

```bash
git clone https://gitlab.com/ashymee.dev/react-ts-shopping-cart.git
```

go to `react-ts-shopping-cart` directory

```bash
cd react-ts-shopping-cart
```

install packages (dependencies & dev dependencies)

```bash
# using npm
npm install

# or using yarn
yarn install
```

run it

```bash
# using npm
npm run start

# or using yarn
yarn start
```

### What next?

There are so many improvements to do...
