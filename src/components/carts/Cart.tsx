import { CartItemType } from '../../App'
import CartItem from '../cartItems/CartItem'
import { CartWrapper } from './Cart.styles'

type CartItemProps = {
  cartItems: CartItemType[]
  addToCart: (clickedItem: CartItemType) => void
  removeFromCart: (id: number) => void
}

const Cart: React.FC<CartItemProps> = ({ cartItems, addToCart, removeFromCart }) => {
  const calculateTotal = (items: CartItemType[]) => items.reduce((acc: number, item) => acc + item.amount * item.price, 0)

  return (
    <CartWrapper>
      <h2>Your shopping cart</h2>
      {cartItems.length === 0 ? <p>no items in cart </p> : null}
      {cartItems.map((item) => (
        <CartItem key={`cart-${item.id}`} item={item} addToCart={addToCart} removeFromCart={removeFromCart} />
      ))}
      <h2>Total: ${calculateTotal(cartItems).toFixed(2)}</h2>
    </CartWrapper>
  )
}

export default Cart
